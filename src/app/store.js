import { configureStore } from '@reduxjs/toolkit'

import notesReducer from '../features/notes/notesSlice'
import padsReducer from '../features/pads/padsSlice'

export const store = configureStore({
  reducer: {
    pads: padsReducer,
    notes: notesReducer
  }
})
