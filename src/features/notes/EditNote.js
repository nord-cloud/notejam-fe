import React, { useState, useEffect } from 'react'
import { Modal, Button, Form } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'

import { selectEditingNoteModal, editNoteAsync } from './notesSlice'

export function EditNote(props) {
  const dispatch = useDispatch()
  const editingNote = useSelector(selectEditingNoteModal)

  const [noteName, setNoteName] = useState('')
  const [noteValue, setNoteValue] = useState('')

  useEffect(() => {
    if (Object.keys(editingNote).length === 0 && !('index' in editingNote)) {
      return
    }

    setNoteName(editingNote.note.name)
    setNoteValue(editingNote.note.content)
  }, [dispatch, editingNote])

  return (
    <Modal
      {...props}
      aria-labelledby="contained-modal-title-vcenter"
      centered
      size="lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">New Note</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {/* {props.note.name} */}

        <Form>
          <Form.Group controlId="note.name">
            <Form.Label>Name</Form.Label>
            <Form.Control
              onChange={(e) => setNoteName(e.target.value)}
              placeholder="note1..."
              value={noteName}
            />
          </Form.Group>
          <Form.Group controlId="note.body">
            <Form.Label>Note</Form.Label>
            <Form.Control
              as="textarea"
              onChange={(e) => setNoteValue(e.target.value)}
              placeholder="note1..."
              rows={4}
              value={noteValue}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          disabled={noteName.length == 0 || noteValue.length == 0}
          onClick={() =>
            dispatch(
              editNoteAsync({
                note: {
                  name: noteName,
                  content: noteValue,
                  id: editingNote.note.id
                },
                index: editingNote.index
              })
            )
          }
          variant="success"
        >
          Save
        </Button>
        <Button onClick={props.onHide} variant="warning">
          Cancel
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
