import React, { useEffect } from 'react'
import { Button, ListGroup, Card } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'

import { PadSettings } from '../pads/PadSettings'
import {
  closePadSettingsModal,
  showPadSettingsModal,
  selectShowPadSettingsModal,
  selectSelectedPad
} from '../pads/padsSlice'

import { EditNote } from './EditNote'
import { NewNote } from './NewNote'
import styles from './Notes.module.css'
import {
  showNewNoteModal,
  selectNotesList,
  selectShowNewNoteModal,
  closeNewNoteModal,
  closeEditNoteModal,
  showEditNoteModal,
  selectShowEditNoteModal,
  fetchNotesAsync
} from './notesSlice'

export function Notes() {
  const Notes = useSelector(selectNotesList)
  const dispatch = useDispatch()
  const showModal = useSelector(selectShowNewNoteModal)
  const showEditModal = useSelector(selectShowEditNoteModal)
  const showPadSettingsModalBool = useSelector(selectShowPadSettingsModal)

  const selectedPad = useSelector(selectSelectedPad)

  useEffect(() => {
    dispatch(fetchNotesAsync())
  }, [dispatch, selectedPad])

  return (
    <Card
      className={styles.container}
      hidden={
        Object.keys(selectedPad).length === 0 && !('index' in selectedPad)
      }
    >
      <div>
        <h4 className={styles.notesListLabel}>Notes:</h4>
        <Button
          className={styles.padSettingsButton}
          onClick={() => {
            dispatch(showPadSettingsModal())
          }}
          variant="info"
        >
          Pad Settings
        </Button>
      </div>

      <div className={styles.NotesList}>
        <ListGroup>
          {Notes.map((note, index) => (
            <ListGroup.Item
              action
              className={styles.note}
              // href={'#' + note.id}
              key={note.id}
              onClick={() => {
                dispatch(showEditNoteModal({ note, index }))
              }}
            >
              {note.name}
            </ListGroup.Item>
          ))}
        </ListGroup>
      </div>
      <div className={styles.NewNotebutton}>
        <Button
          block
          onClick={() => {
            dispatch(showNewNoteModal())
          }}
        >
          Add note
        </Button>
      </div>

      <NewNote onHide={() => dispatch(closeNewNoteModal())} show={showModal} />

      <EditNote
        onHide={() => dispatch(closeEditNoteModal())}
        show={showEditModal}
      />

      <PadSettings
        onHide={() => dispatch(closePadSettingsModal())}
        show={showPadSettingsModalBool}
      />
    </Card>
  )
}
