import React, { useState, useEffect } from 'react'
import { Modal, Button, Form } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { v4 as uuidv4 } from 'uuid'

import { createNoteAsync, selectShowNewNoteModal } from './notesSlice'

export function NewNote(props) {
  const dispatch = useDispatch()

  const showModal = useSelector(selectShowNewNoteModal)
  const [noteName, setNoteName] = useState('')
  const [noteValue, setNoteValue] = useState('')

  useEffect(() => {
    // TODO: Find a way to keep the state of the newNote text if the user presses cancel
    //       Should be kept unless the pad selected has changed
    setNoteName('')
    setNoteValue('')
  }, [dispatch, showModal])

  return (
    <Modal
      {...props}
      aria-labelledby="contained-modal-title-vcenter"
      centered
      size="lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">New Note</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {/* {props.note.name} */}

        <Form>
          <Form.Group controlId="note.name">
            <Form.Label>Name</Form.Label>
            <Form.Control
              onChange={(e) => setNoteName(e.target.value)}
              placeholder="note1..."
              value={noteName}
            />
          </Form.Group>
          <Form.Group controlId="note.value">
            <Form.Label>Note</Form.Label>
            <Form.Control
              as="textarea"
              onChange={(e) => setNoteValue(e.target.value)}
              placeholder="note1..."
              rows={4}
              value={noteValue}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          disabled={noteName.length == 0 || noteValue.length == 0}
          onClick={() =>
            dispatch(
              createNoteAsync({
                name: noteName,
                content: noteValue,
                id: uuidv4()
              })
            )
          }
          variant="success"
        >
          Save
        </Button>
        <Button onClick={props.onHide} variant="warning">
          Cancel
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
