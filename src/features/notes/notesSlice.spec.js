import notesReducer, {
  increment,
  decrement,
  incrementByAmount
} from './notesSlice'

describe('notes reducer', () => {
  const initialState = {
    value: 3,
    status: 'idle'
  }

  it('should handle initial state', () => {
    expect(notesReducer(undefined, { type: 'unknown' })).toEqual({
      value: 0,
      status: 'idle'
    })
  })

  it('should handle increment', () => {
    const actual = notesReducer(initialState, increment())

    expect(actual.value).toEqual(4)
  })

  it('should handle decrement', () => {
    const actual = notesReducer(initialState, decrement())

    expect(actual.value).toEqual(2)
  })

  it('should handle incrementByAmount', () => {
    const actual = notesReducer(initialState, incrementByAmount(2))

    expect(actual.value).toEqual(5)
  })
})
