import React, { useState, useEffect } from 'react'
import { Button, ListGroup } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import { v4 as uuidv4 } from 'uuid'

import styles from './Pads.module.css'
import {
  createPadAsync,
  selectPadsList,
  selectedPad,
  fetchPadsAsync
} from './padsSlice'

export function Pads() {
  const pads = useSelector(selectPadsList)
  const dispatch = useDispatch()
  const [padNameValue, setPadNameValue] = useState('')

  useEffect(() => {
    dispatch(fetchPadsAsync())
  }, [dispatch])

  return (
    <div className={styles.container}>
      <h4 className={styles.padsListLabel}>Pads:</h4>
      <ListGroup className={styles.padsList}>
        {' '}
        {/* defaultActiveKey="#link1"> */}
        {pads.map((pad, index) => (
          <ListGroup.Item
            action
            className={styles.pad}
            href={'#' + pad.id}
            key={pad.id}
            onClick={() => {
              dispatch(selectedPad({ id: pad.id, index }))
            }}
          >
            {pad.name}
          </ListGroup.Item>
        ))}
      </ListGroup>

      <div className={styles.row}>
        <input
          aria-label="Set pad name"
          className={styles.textbox}
          onChange={(e) => setPadNameValue(e.target.value)}
          value={padNameValue}
        />
        <Button
          className={styles.addPaddButton}
          onClick={() => {
            if (padNameValue.length > 0) {
              dispatch(
                createPadAsync({
                  name: padNameValue,
                  id: uuidv4()
                })
              )
              setPadNameValue('')
            }
          }}
          size="sm"
        >
          Add Pad
        </Button>
      </div>
    </div>
  )
}
