import React, { useState, useEffect } from 'react'
import { Modal, Button, Form } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'

import { selectSelectedPad, editPadAsync, selectPadsList } from './padsSlice'

export function PadSettings(props) {
  const dispatch = useDispatch()
  const selectedPad = useSelector(selectSelectedPad)
  const padsList = useSelector(selectPadsList)
  const [padName, setPadName] = useState('')

  useEffect(() => {
    if (Object.keys(selectedPad).length === 0 && !('index' in selectedPad)) {
      return
    }

    setPadName(padsList[selectedPad.index].name)
  }, [dispatch, selectedPad, padsList])

  return (
    <Modal
      {...props}
      aria-labelledby="contained-modal-title-vcenter"
      centered
      size="lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Edit pad</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId="pad.name">
            <Form.Label>Name</Form.Label>
            <Form.Control
              onChange={(e) => setPadName(e.target.value)}
              value={padName}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          disabled={padName.length == 0}
          onClick={() =>
            dispatch(
              editPadAsync({
                pad: {
                  name: padName,
                  id: selectedPad.id
                },
                index: selectedPad.index
              })
            )
          }
          variant="success"
        >
          Save
        </Button>
        <Button onClick={props.onHide} variant="warning">
          Cancel
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
