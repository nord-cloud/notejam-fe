import padsReducer, {
  increment,
  decrement,
  incrementByAmount
} from './padsSlice'

describe('pads reducer', () => {
  const initialState = {
    value: 3,
    status: 'idle'
  }

  it('should handle initial state', () => {
    expect(padsReducer(undefined, { type: 'unknown' })).toEqual({
      value: 0,
      status: 'idle'
    })
  })

  it('should handle increment', () => {
    const actual = padsReducer(initialState, increment())

    expect(actual.value).toEqual(4)
  })

  it('should handle decrement', () => {
    const actual = padsReducer(initialState, decrement())

    expect(actual.value).toEqual(2)
  })

  it('should handle incrementByAmount', () => {
    const actual = padsReducer(initialState, incrementByAmount(2))

    expect(actual.value).toEqual(5)
  })
})
