import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react'
import React from 'react'

import 'bootstrap/dist/css/bootstrap.min.css'
// import { NewNote } from './features/notes/NewNote'
import { Notes } from './features/notes/Notes'
import { Pads } from './features/pads/Pads'

import './App.css'

function App() {
  return (
    <>
      <div className="signoutButton">
        <AmplifySignOut />
      </div>
      <div className="App">
        <Pads />
        <Notes />
      </div>
    </>
  )
}

export default withAuthenticator(App)
