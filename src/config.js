const API_CONFIGURATION = process.env.REACT_APP_API_CONFIGURATION
  ? process.env.REACT_APP_API_CONFIGURATION
  : 'https://dev.api.tingirica.com'

var config = {}

// if (process.env.NODE_ENV !== 'production') { // FIXME: add different endpoints for different envs
config = {
  API_CONFIGURATION,
  apiGateway: {
    REGION: 'eu-central-1',
    URL: API_CONFIGURATION
  },
  cognito: {
    REGION: 'eu-central-1',
    USER_POOL_ID: 'eu-central-1_cEviElcwP',
    APP_CLIENT_ID: '13m72lfjk8o500kr2rdi9h0e9m',
    IDENTITY_POOL_ID: 'eu-central-1:5e6a3463-5af6-4156-8ade-a82a4a7c0d3a'
  }
}
// }

export default config
